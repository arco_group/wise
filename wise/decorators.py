# -*- mode: python; coding: utf-8 -*-

def use_post(fn):
    fn.use_post = True
    return fn
